from pathlib import Path
import argparse
from PIL import Image


class PNGToP8Converter:

    TILE_SIZE = 8
    NOT_FOUND_INDEX = 0

    NORMAL_KEY = 'normal'
    FLIP_X_KEY = 'flip_x'
    FLIP_Y_KEY = 'flip_y'
    FLIP_XY_KEY = 'flip_xy'

    MAP_FUCNTIONS_FILE_NAME = 'map-functions.lua'

    @staticmethod
    def convert_map_indexes_to_map_string(map_tile_indexes):
        result_lines = []
        for row in map_tile_indexes:
            row_string = ''
            for tile in row:
                row_string += f'{tile:02x}'
            result_lines.append(row_string)

        return result_lines

    def __init__(self, input_map_filename, input_tiles_filename, tiles_offset, p8_output_filename, map_code_output_filename):
        self.input_map_filename = input_map_filename
        self.input_tiles_filename = input_tiles_filename
        self.tiles_offset = tiles_offset
        self.p8_output_filename = p8_output_filename
        self.map_code_output_filename = map_code_output_filename
        self.tiles = []
        self.flips_list = []

        with Path(self.MAP_FUCNTIONS_FILE_NAME).open('r') as functions_code_file:
            self.map_functions_code = functions_code_file.read()

    @staticmethod
    def get_png_pixels(png_filename):
        image = Image.open(Path(png_filename))
        image_pixels = image.convert('RGB').load()

        cols = image.size[0] // PNGToP8Converter.TILE_SIZE
        rows = image.size[1] // PNGToP8Converter.TILE_SIZE

        return image_pixels, cols, rows

    def create_tile(self, pixels, col, row, with_flips=False):
        normal_tile = []
        for i in range(self.TILE_SIZE):
            tile_row = []
            for j in range(self.TILE_SIZE):
                tile_row.append(pixels[col * self.TILE_SIZE + j, row * self.TILE_SIZE + i])
            normal_tile.append(tile_row)

        if with_flips:
            flip_y_tile = normal_tile[::-1]
            flip_x_tile = [row[::-1] for row in normal_tile]
            flip_xy_tile = flip_x_tile[::-1]

            return {self.NORMAL_KEY: normal_tile, self.FLIP_X_KEY: flip_x_tile,
                    self.FLIP_Y_KEY: flip_y_tile, self.FLIP_XY_KEY: flip_xy_tile}

        return normal_tile

    def load_tiles(self):
        input_tiles_pixels, cols, rows = self.get_png_pixels(self.input_tiles_filename)

        print(f'Loaded {cols * rows} tiles in grid of {cols}*{rows}')

        for row in range(rows):
            for col in range(cols):
                tile = self.create_tile(input_tiles_pixels, col, row, True)
                self.tiles.append(tile)

    def convert_map_to_tiles(self):
        input_map_pixels, cols, rows = self.get_png_pixels(self.input_map_filename)

        print(f'Loaded {cols * rows} tiles in map of {cols}*{rows}')

        map_indexes = []
        specials_list = []

        for row in range(rows):
            row_indexes = []
            for col in range(cols):
                found_tile = False
                map_tile = self.create_tile(input_map_pixels, col, row)

                for tile_index in range(len(self.tiles)):
                    if found_tile:
                        break
                    for key, tile in self.tiles[tile_index].items():
                        if tile == map_tile:
                            found_tile = True
                            row_indexes.append(tile_index)
                            if key != self.NORMAL_KEY:
                                specials_list.append((row, col, key, tile_index))
                            break

                if not found_tile:
                    row_indexes.append(self.NOT_FOUND_INDEX)
            map_indexes.append(row_indexes)

        return map_indexes, specials_list

    def write_map_to_output(self, map_indexes_lines):
        with Path(self.p8_output_filename).open() as p8_file:
            p8_file_lines = p8_file.readlines()

        terminated_map_indexes_lines = [index + '\n' for index in map_indexes_lines]

        map_start_string = '__map__\n'
        map_line_index = p8_file_lines.index('__map__\n') if map_start_string in p8_file_lines else None

        if map_line_index is None:
            if len(p8_file_lines) > 0 and p8_file_lines[-1].find('\n') != len(p8_file_lines[-1]) - 1:
                p8_file_lines[-1] += '\n'
            p8_file_lines.append('__map__\n')
            p8_file_lines.extend(terminated_map_indexes_lines)
        else:
            next_section_index = None
            for index in range(map_line_index + 1, len(p8_file_lines)):
                if p8_file_lines[index].find('__') == 0:
                    next_section_index = index
                    break

            if next_section_index is None:
                p8_file_lines[map_line_index + 1:] = terminated_map_indexes_lines
            else:
                p8_file_lines[map_line_index + 1:next_section_index] = terminated_map_indexes_lines

        with Path(self.p8_output_filename).open('w') as p8_out_file:
            p8_out_file.writelines(p8_file_lines)

        print('')
        print('__map__')
        for map_line in map_indexes_lines:
            print(map_line)

    def write_to_file_and_screen(self, file, line):
        file.write(line + '\n')
        print(line)

    def write_map_code_to_output(self, specials_list):
        with Path(self.map_code_output_filename).open('w') as code_file:
            self.write_to_file_and_screen(code_file, '___specials_list = {')
            for index, (row, col, key, tile_index) in enumerate(specials_list):
                is_flip_x = 'true' if key in [self.FLIP_X_KEY, self.FLIP_XY_KEY] else 'false'
                is_flip_y = 'true' if key in [self.FLIP_Y_KEY, self.FLIP_XY_KEY] else 'false'
                line = f'\t{{tile_index={tile_index}, map_col={col}, map_row={row}, width={self.TILE_SIZE}, height={self.TILE_SIZE}, is_flip_x={is_flip_x}, is_flip_y={is_flip_y}}}'
                if index < len(specials_list) - 1:
                    line += ','
                self.write_to_file_and_screen(code_file, line)
            self.write_to_file_and_screen(code_file, '}')

            self.write_to_file_and_screen(code_file, '')
            self.write_to_file_and_screen(code_file, self.map_functions_code)

    def convert(self):
        self.load_tiles()
        map_tile_indexes, specials_list = self.convert_map_to_tiles()
        map_lines = self.convert_map_indexes_to_map_string(map_tile_indexes)
        self.write_map_to_output(map_lines)
        self.write_map_code_to_output(specials_list)


def get_args():
    parser = argparse.ArgumentParser(description='Converts map + tiles PNG files to a p8 Pico-8 Cartridge file format.')
    parser.add_argument('map_png', metavar='map-png', help='map png filename')
    parser.add_argument('tiles_png', metavar='tiles-png', help='tiles png filename')
    parser.add_argument('--p8_tiles_offset', metavar='p8-tiles-offset', type=int, default=0,
                        help='p8 output file tiles offset to append from (default: 0)')
    parser.add_argument('--p8_output', metavar='p8-output', default='png2p8-output.p8',
                        help='output p8 file (default: png2p8-output.p8)')
    parser.add_argument('--map_code_output', metavar='map-code-output', default='map.lua',
                        help='output map code file (default: map.lua)')

    args = parser.parse_args()

    return args


def main():
    args = get_args()

    converter = PNGToP8Converter(args.map_png, args.tiles_png, args.p8_tiles_offset, args.p8_output, args.map_code_output)
    converter.convert()


if __name__ == '__main__':
    main()
