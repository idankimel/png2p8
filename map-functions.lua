function _draw_map_specials(start_col, start_row, end_col, end_row, start_x, start_y)
 for i = 1,#___specials_list do
  special = ___specials_list[i]
  if special.map_col >= start_col and special.map_col <= end_col and special.map_row >= start_row and special.map_row <= end_row then
   row = special.map_row - start_row
   col = special.map_col - start_col
   spr(special.tile_index, start_x + col * special.width, start_y + row * special.height, 1, 1, special.is_flip_x, special.is_flip_y)
  end
 end
end